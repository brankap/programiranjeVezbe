
import com.mycompany.programiranjevezbe.DivisionOfNumbers;
import java.util.List;

import org.junit.Test;
import static org.assertj.core.api.Assertions.assertThat;


/**
 *
 * @author branka
 */
public class DivisionOfNumbersTest {
    
    DivisionOfNumbers divisionOfNumbers= new DivisionOfNumbers();
    
    
    @Test
    public void validateDividedNumbers(){
       List<Integer> listOfNumbers = divisionOfNumbers.findDivisibleNumbers(14, 3);
       assertThat(listOfNumbers).containsExactlyInAnyOrder(3,6,9,12);
    
    }
    
    @Test
    public void validateDividedNumbersBetweenTwoNumbers(){
       List<Integer> listOfNumbers = divisionOfNumbers.findDivisibleNumbersBetweenTwoNumbers(14,20, 3);
       assertThat(listOfNumbers).containsExactlyInAnyOrder(15,18);
    
    }
}
