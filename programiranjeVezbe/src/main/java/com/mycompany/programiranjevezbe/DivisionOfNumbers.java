package com.mycompany.programiranjevezbe;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author branka
 */
public class DivisionOfNumbers {

    List<Integer> listOfNumbers = new ArrayList<>();

    public List<Integer> findDivisibleNumbers(int n, int k) {
        return findDivisibleNumbersBetweenTwoNumbers(1, n, k);
    }

    public List<Integer> findDivisibleNumbersBetweenTwoNumbers(int m, int n, int k) {
        for (int i = m; i < n; i++) {
            if (i % k == 0) {
                listOfNumbers.add(i);
            }
        }
        return listOfNumbers;
    }
}
